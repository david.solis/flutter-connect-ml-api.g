import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_diabetes/widgets/custom_slider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:http/http.dart' as http;

import '../widgets/custom_text_form_field.dart';

class DiabetesInterface extends StatefulWidget {
  const DiabetesInterface({Key? key}) : super(key: key);

  @override
  State<DiabetesInterface> createState() => _DiabetesInterfaceState();
}

class _DiabetesInterfaceState extends State<DiabetesInterface> {
  double _embarazos_value = 0;
  double _edad_value = 1;
  Map<String, double> sendValues = {
    "embarazos": 0,
    "glucosa": 0,
    "presion_arterial": 0,
    "espesor_piel": 0,
    "insulina": 0,
    "imc": 0,
    "diabetes_pedigree_function": 0,
    "edad": 0
  };

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.indigo, title: const Text('Diabetes ML')),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              Container(
                height: 120,
                child: const Image(
                  image: AssetImage('assets/diabetes-test.png'),
                ),
              ),
              SizedBox(height: 30),
              //Text('Número de Embarazos: $_value'),
              CustomSlider(
                min: 0,
                max: 15,
                divisions: 15,
                sendValues: sendValues,
                property: 'embarazos',
                label: 'Número de Embarazos: ',
              ),
              CustomTextFormField(
                  labelText: 'Glucosa',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'glucosa',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Presión Arterial',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'presion_arterial',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Espesor Piel',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'espesor_piel',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Insulina',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'insulina',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Indice Masa Corporal',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'imc',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Función Diabetes Pedigree',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'diabetes_pedigree_function',
                  formValues: sendValues),
              SizedBox(height: 30),
              CustomSlider(
                min: 0,
                max: 120,
                divisions: 120,
                sendValues: sendValues,
                property: 'edad',
                label: 'Edad: ',
              ),
              SizedBox(height: 30),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.indigo, shape: StadiumBorder()),
                  onPressed: () async {
                    if (!formKey.currentState!.validate()) {
                      //FocusScope.of(context).requestFocus(FocusNode());
                      return;
                    }
                    print(sendValues);
                    final url =
                        Uri.http("127.0.0.1:8000", "/diabetes-predictions");
                    final response = await http.post(url,
                        body: json.encode(sendValues),
                        headers: {
                          'Content-Type': "application/json; charset=utf-8"
                        });
                    print('Code Status: ${response.statusCode}');
                    print('Response Body: ${response.body}');
                    var has_diabetes = "";
                    if (response.statusCode == 201) {
                      final jsonResponse = jsonDecode(response.body);
                      has_diabetes = (jsonResponse['tiene_diabetes'])
                          ? 'POSITIVO'
                          : 'NEGATIVO';
                    }

                    Alert(
                      context: context,
                      type: (has_diabetes == 'POSITIVO')
                          ? AlertType.success
                          : AlertType.error,
                      title: "",
                      desc:
                          "Los datos ingresados infieren un caso de Diabetes $has_diabetes!!",
                      buttons: [
                        DialogButton(
                          child: const Text(
                            "OK",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () => Navigator.pop(context),
                          width: 120,
                        )
                      ],
                    ).show();
                  },
                  child: const SizedBox(
                      width: double.infinity,
                      child: Center(child: Text('Comprobar'))))
            ],
          ),
        ),
      )),
    );
  }
}
