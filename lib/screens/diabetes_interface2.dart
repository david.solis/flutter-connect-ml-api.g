import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:http/http.dart' as http;

import '../widgets/custom_text_form_field.dart';

class DiabetesInterface extends StatelessWidget {
  const DiabetesInterface({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Map<String, double> sendValues = {
      "embarazos": 0,
      "glucosa": 0,
      "presion_arterial": 0,
      "espesor_piel": 0,
      "insulina": 0,
      "imc": 0,
      "diabetes_pedigree_function": 0,
      "edad": 0
    };
    return Scaffold(
      appBar: AppBar(title: const Text('Diabetes ML')),
      body: Center(
          child: SingleChildScrollView(
              child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 0),
        child: Column(
          children: [
            Text('Número de Embarazos:'),
            Slider(value: 3, min: 0, max: 100, onChanged: (context) {}),
            /*TextFormField(
              initialValue: '',
              decoration: const InputDecoration(labelText: 'Glucosa'),
            )*/
            CustomTextFormField(
                labelText: 'Glucosa',
                hintText: '0.0',
                keyboardType: TextInputType.number,
                formProperty: 'glucosa',
                formValues: sendValues),
            CustomTextFormField(
                labelText: 'Presión Arterial',
                hintText: '0.0',
                keyboardType: TextInputType.number,
                formProperty: 'presion_arterial',
                formValues: sendValues),
            CustomTextFormField(
                labelText: 'Espesor Piel',
                hintText: '0.0',
                keyboardType: TextInputType.number,
                formProperty: 'espesor_piel',
                formValues: sendValues),

            /* TextFormField(
              initialValue: '',
              decoration: const InputDecoration(labelText: 'Presión Arterial'),
            ),
            TextFormField(
              initialValue: '',
              decoration: const InputDecoration(labelText: 'Espesor Piel'),
            ),*/
            /*TextFormField(
              initialValue: '',
              decoration: const InputDecoration(labelText: 'Insulina'),
            ),*/
            CustomTextFormField(
                labelText: 'Insulina',
                hintText: '0.0',
                keyboardType: TextInputType.number,
                formProperty: 'insulina',
                formValues: sendValues),
            /*TextFormField(
              initialValue: '',
              decoration:
                  const InputDecoration(labelText: 'Indice Masa Corporal'),
            ),*/
            CustomTextFormField(
                labelText: 'Indice Masa Corporal',
                hintText: '0.0',
                keyboardType: TextInputType.number,
                formProperty: 'imc',
                formValues: sendValues),
            /*TextFormField(
              initialValue: '',
              decoration:
                  const InputDecoration(labelText: 'Función Diabetes Pedigree'),
            ),*/

            CustomTextFormField(
                labelText: 'Función Diabetes Pedigree',
                hintText: '0.0',
                keyboardType: TextInputType.number,
                formProperty: 'diabetes_pedigree_function',
                formValues: sendValues),
            SizedBox(height: 30),
            Text('Edad:'),
            Slider(value: 3, min: 1, max: 120, onChanged: (context) {}),
            SizedBox(height: 30),
            ElevatedButton(
                onPressed: () async {
                  final url =
                      Uri.http("127.0.0.1:8000", "/diabetes-predictions");
                  final response = await http.post(url,
                      body: json.encode(sendValues),
                      headers: {
                        'Content-Type': "application/json; charset=utf-8"
                      });
                  print('Code Status: ${response.statusCode}');
                  print('Response Body: ${response.body}');
                  var has_diabetes = "";
                  if (response.statusCode == 201) {
                    final jsonResponse = jsonDecode(response.body);
                    has_diabetes = (jsonResponse['tiene_diabetes'])
                        ? 'POSITIVO'
                        : 'NEGATIVO';
                  }

                  Alert(
                    context: context,
                    type: (has_diabetes == 'POSITIVO')
                        ? AlertType.success
                        : AlertType.error,
                    title: "",
                    desc:
                        "Los datos ingresados infieren un caso de Diabetes $has_diabetes!!",
                    buttons: [
                      DialogButton(
                        child: const Text(
                          "OK",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 120,
                      )
                    ],
                  ).show();
                },
                child: const SizedBox(
                    width: double.infinity,
                    child: Center(child: Text('Comprobar'))))
          ],
        ),
      ))),
    );
  }
}
